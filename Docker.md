# DOCKER

## Qu'est-ce qu'une machine virtuelle ?

Les machines virtuelles sont une abstraction du matériel physique qui transforme un serveur en plusieurs serveurs. Chaque VM comprend une copie complète du système d'exploitation, de l'application, des binaires et des bibliothèques nécessaires, ce qui occupe des dizaines de Go. 

## Qu'est-ce que Docker, et quelle est la différence avec une machine virtuelle ?

Docker est un ensemble de produit PaaS (Plateform as a Service) qui utilisent la virtualisation au niveau du système pour fournir des logiciels dans des paquets (packages) appelés containers.

Contrairement à une machine virtuelle, il ne s'agit pas de virtualisation, mais de conteneurisation, une forme plus légère qui s'appuie sur certaines parties de la machine hôte pour son fonctionnement, tel que le noyau du système d'exploitation.

Les principaux avantages des conteneurs sont : 
- Ils sont portables : ils fonctionnent sur n'importe quels OS, serveur, cloud.
- Ils permettent de mettre en place un environnement spécifique au projet sans modifier les configurations de son environnement de travail.
- Ils permettent de séparer les différentes parties d'un gros projet.
- Ils ne nécessitent pas d’allocation de ressource physique.
- Ils démarrent plus vite.
- Ils permettent aux entreprises d’emballer d'avantages d’application dans un seul serveur physique.

## Qu'est-ce qu'une image Docker ?

Une image de conteneur Docker est un ensemble de logiciels légers, autonomes et exécutables qui comprend tout ce qui est nécessaire à l'exécution d'une application : le code, le moteur d'exécution, les outils système, les bibliothèques système et les paramètres.

## Qu'est-ce qu'un conteneur (container) Docker ?

Un conteneur est une unité logicielle standard qui regroupe le code et toutes ses dépendances afin que l'application s'exécute rapidement et de manière fiable d'un environnement informatique à l'autre. 

## Qu'est-ce que Docker Hub ?

Docker Hub est un registre Docker hébergé géré par Docker. Docker Hub contient plus de 100 000 images de conteneurs provenant d'éditeurs de logiciels, de projets open source et de la communauté.

# DOCKER (La Suite !)

## Les données persistent-elles lorsque qu'on stoppe et redémarre un conteneur ?

Non.

## Qu'est-ce qu'un volume ?

Un volume dans Docker est un type de stockage qui permet de persister les données d’un conteneur (par défaut aucune donnée n’est persistée). Ce qui signifie que s’il n’y a pas la présence de volume, un conteneur perd toutes ses données lorsqu’il est arrêté.

Dans Docker on retrouve 3 types de stockages :

- Volume
- Bind mounts
- Tmpfs mount

Une fois un volume créé, le client aura juste à démarrer le conteneur dans le volume et les données de ce conteneur seront persistées.

## Qu'est-ce qu'un bind mount, et quelle est la différence avec un volume ?

Un volume de type "bind mount" est un moyen pour les containers Docker de se lier aux fichiers ou aux répertoires de l'hôte. Contrairement aux volumes de type "volume", les volumes de type "bind mount" ne sont pas persistants et ne sont pas gérés par Docker. Les volumes de type "bind mount" peuvent être utilisés pour monter des fichiers de configuration, des fichiers journaux ou d'autres fichiers qui ne nécessitent pas de persistance.
Le conteneur voit immédiatement les modifications que vous apportez au code, dès que vous enregistrez un fichier. Cela signifie que vous pouvez exécuter des processus dans le conteneur qui surveillent les modifications du système de fichiers et y répondent.